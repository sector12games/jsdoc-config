// Jsdoc allows config files of either .json or .js format. For future reference, after
// reviewing the source code, I've confirmed that jsdoc strips any comments in .json
// files before loading the json object. This means that if I did ever want to switch
// to .json format, it would be perfectly acceptable to annotate this file with
// javascript-style comments. It's a shame npm doesn't allow this. To see the default
// config values used by jsdoc, see the following:
// http://usejsdoc.org/about-configuring-jsdoc.html#default-configuration-options
module.exports = {
    source: {
        include: [ 'README.md', 'src' ],
        exclude: [],

        includePattern: '.+\\.js(doc|x)?$',
        excludePattern: '(^|\\/|\\\\)_',
    },
    opts: {
        destination: './jsdoc',
        recurse: true,
    },
    plugins: [
        // This plugin is built in to jsdoc, will convert .md files to html
        'plugins/markdown',
    ],
    templates: {
        // Overriding new config values provided by ink-docstrap. Whatever is not
        // overridden here will be pulled from:
        // node_modules/ink-docstrap/template/jsdoc.conf.json
        theme: 'flatly',
        navType: 'vertical',
        linenums: true,
        dateFormat: 'MMMM Do YYYY, h:mm:ss a',
    }
};
